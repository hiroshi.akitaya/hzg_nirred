# HZG Photomety pipeline

## Description
- hzg_nirred_mkmodelimage_old.ipynb
-- Create dithered images from the original model image.
- hzg_nirred_model_1.ipynb
-- Main routine for NIR images photometry.

## Directories
- ./model: Original model images.
- ./dither_fits: Dithered images processed by hzg_nirred_mkmodelimage_old.ipynb.
- ./result: Result data processed by hzg_nirred_model_1.ipynb.

## Copyright
- Hiroshi AKITAYA (PERC/CIT), Yoshifusa ITA (Tohoku Univ.), and HiZ-GUNDAM team.
